<?php
  class Usuario extends CI_Model{
    //Funcion para autenticar usuarios -> Email y Contrase
    public function consultarPorEmailPassword($email,$password){
      $this->db->where('correo_usu',$email);
      $this->db->where('password_usu',$password);
      $query=$this->db->get("usuario");
      if ($query->num_rows()>0) {
        return $query->row();
      }else {
        return false;
      }
    }
    public function insertarUsuario($data){
      return $this->db->insert('usuario',$data);
    }

    public function obtenerDatos(){
      $query=$this->db->get('usuario');
      if ($query->num_rows()>0) {
        return $query;
      }else{
        return false;
      }
    }

    public function obtenerPorId($id){
      $this->db->where('id_usu',$id);
      $query=$this->db->get('usuario');
      if($query->num_rows()>0){
        return $query->row();
      }else{
        return false;
      }
    }

    public function actualizarUsuario($id,$datosUsuario){
      $this->db->where('id_usu',$id);
      return $this->db->update('usuario',$datosUsuario);

    }

    public function eliminarUsuario($id){
      $this->db->where('id_usu',$id);
      return $this->db->delete('usuario');
    }
  }
?>
