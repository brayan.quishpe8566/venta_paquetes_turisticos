<div class="row">
  <div class="col-md-12 text-center">
    <legend>
      <h3>
        <i class="glyphicon glyphicon-plus"></i>
        <b>REGISTRO DE NUEVO USUARIO</b>
      </h3>
    </legend>

  </div>

</div>

<div class="row">
  <div class="col-md-4">

  </div>
  <div class="col-md-4">

  </div>
  <div class="col-md-4 text-center" >
    <a class="btn btn-success" role="button" href="<?php echo site_url() ?>/usuarios/listadoUsuario">
       LISTADO
       <i class="glyphicon glyphicon-th-list"></i>
     </a>
  </div>


</div>
<div class="row">
  <div class="col-md-4">
  </div>
  <div class="col-md-4">
    <form class="" action="<?php echo site_url(); ?>/usuarios/insertarUsuario" method="post">
      <br>
      <label for="">Email:</label><br>
      <input class="form-control" placeholder="Ingrese el email de usuario" required type="email" name="correo_usu" id="correo_usu" value=""><br>
      <label for="">Password:</label><br>
      <input class="form-control" type="password" name="password_usu" id="password_usu" placeholder="Ingrese una contraseña" required value=""><br>
      <label for="">Perfil:</label>
      <select class="form-control" type="" name="perfil_usu" id="perfil_usu"><br><br>
        <option value="">--Seleccione--</option>
        <option value="1" disabled>Administrador</option>
        <option value="2">Secretari@</option>
      </select>
      <br><br>
      <div class="text-center">
        <button type="submit" name="button" class="btn btn-primary ">GUARDAR USUARIO</button>
        <a href="<?php echo site_url(); ?>" class="btn btn-danger">CANCELAR</a>
      </div>
    </form><br>

  </div>
  <div class="col-md-4">

  </div>

</div>
