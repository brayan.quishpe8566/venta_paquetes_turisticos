<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <legend>
        <h3>
          <b>LISTADO DE USUARIOS</b>
        </h3>
      </legend>

    </div>

  </div>
  <br><br>
  <div class="row">
    <div class="col-md-2">

    </div>
    <div class="col-md-6">
      <?php if ($listadoUsuarios): ?>
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th class="text-center">CODIGO</th>
              <th class="text-center">EMAIL</th>
              <th class="text-center">PASSWORD</th>
              <th class="text-center">PERFIL</th>
              <!-- <th class="text-center">ESTADO</th> -->
              <th class="text-center">ACCIONES</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($listadoUsuarios->result() as $usuarioTemporal): ?>
              <tr>
                <td class="text-center"><?php echo $usuarioTemporal->id_usu ?></td>
                <td class="text-center"><?php echo $usuarioTemporal->correo_usu ?></td>
                <td class="text-center"><?php echo $usuarioTemporal->password_usu ?></td>
                <td class="text-center"><?php echo $usuarioTemporal->perfil_usu ?></td>
                <!-- <td class="text-center"><?php echo $usuarioTemporal->estado_usu ?></td> -->
                <td class="text-center">
                  <a href="<?php echo site_url(); ?>/usuarios/editarUsuario/<?php echo $usuarioTemporal->id_usu; ?>">
                    <i class="glyphicon glyphicon-pencil" title="editar"></i>
                  </a>
                  <a href="<?php echo site_url(); ?>/usuarios/eliminarUsuario/<?php echo $usuarioTemporal->id_usu; ?>"
                    onclick="confirmation(event)" >
                    <i class="glyphicon glyphicon-trash" title="editar"></i>
                  </a>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>

        </table>
      <?php else: ?>
        <div class="alet alert-danger">
          <br><br><br>
          no se encuentra usuarios registrado
        </div>
      <?php endif; ?>
    </div>
    <div class="col-md-4">

    </div>

  </div>
  <a href="<?php echo site_url()	?>" class="btn btn-danger btn-lg active" role="button" aria-pressed="true" >CANCELAR</a>
</div>

<script>
function confirmation(ev) {
   ev.preventDefault();
   var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
   console.log(urlToRedirect); // verify if this is the right URL
   Swal.fire({
  title: '¿Estas seguro?',
  text: "¡Esto sera permanente!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '¡Borralo!',
  cancelButtonText:'Cancelar'
}).then((result) => {
  if (result.isConfirmed) {
    window.location.href = urlToRedirect;
  }
});
}
</script>
