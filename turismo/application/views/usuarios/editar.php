<div class="row">
  <div class="col-md-12 text-center">
    <legend>
      <h3>
        <i class="glyphicon glyphicon-plus"></i>
        <b>EDITAR USUARIO</b>
      </h3>
    </legend>

  </div>

</div>

<div class="row">
  <div class="col-md-4">

  </div>
  <div class="col-md-4">

  </div>
  <div class="col-md-4 text-center" >
    <a class="btn btn-success" role="button" href="<?php echo site_url() ?>/usuarios/listadoUsuario">
       LISTADO
       <i class="glyphicon glyphicon-th-list"></i>
     </a>
  </div>


</div>
<div class="row">
  <div class="col-md-4">
  </div>
  <div class="col-md-4">
    <form class="" action="<?php echo site_url(); ?>/usuarios/actualizarUsuario" method="post">
      <br>
      <input type="hidden" name="id_usu" id="id_usu" value="<?php echo $editarUsuarios->id_usu; ?>">
      <label for="">Email:</label><br>
      <input class="form-control" placeholder="Ingrese el email de usuario" required type="email" name="correo_usu" id="correo_usu" value="<?php echo $editarUsuarios->correo_usu ?>"><br>
      <label for="">Password:</label><br>
      <input  class="form-control" type="password" name="password_usu" id="password_usu" placeholder="actualizarcontraseña" required value="<?php echo $editarUsuarios->password_usu; ?>"><br>
      <label for="">Perfil:</label>
      <input class="form-control" type="text" name="perfil_usu" id="perfil_usu" placeholder="Ingrese el perfil de usuario" required value="<?php echo $editarUsuarios->perfil_usu ?>"><br>
      <label for="">Estado:</label>
      <!-- <select class="form-control" name="estado_usu" id="estado_usu">

        <option value="1">Activo</option>
        <option value="0">Inactivo</option>
      </select> -->
      <br><br>
      <div class="text-center">
        <button type="submit" name="button" class="btn btn-primary ">GUARDAR USUARIO</button>
        <a href="<?php echo site_url(); ?>" class="btn btn-danger">CANCELAR</a>
      </div>
    </form><br>

  </div>
  <div class="col-md-4">

  </div>

</div>
<script>
  $('#estado_usu').val("<?php echo $editarUsuarios->estado_usu;?>")
</script>
