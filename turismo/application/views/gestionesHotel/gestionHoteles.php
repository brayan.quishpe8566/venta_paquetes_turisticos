<?php
foreach($css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />

<?php endforeach; ?>


<?php foreach($js_files as $file): ?>

<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<section class="default-banner active-blog-slider">
					<div class="item-slider relative" style="background: url(<?php echo base_url(); ?>/assets/img/slider1.jpg);background-size: cover;">
						<div class="overlay" style="background: rgba(0,0,0,.3)"></div>
						<div class="container">
							<div class="row fullscreen justify-content-center align-items-center">
								<div class="col-md-10 col-12">
									<div class="banner-content text-center">
										<h4 class="text-white mb-20 text-uppercase">Discover the Colorful World</h4>
										<h1 class="text-uppercase text-white">New Adventure</h1>
										<p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temp <br>
										or incididunt ut labore et dolore magna aliqua. Ut enim ad minim.</p>
										<a href="#" class="text-uppercase header-btn">Discover Now</a>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="item-slider relative" style="background: url(<?php echo base_url(); ?>/assets/img/slider2.jpg);background-size: cover;">
						<div class="overlay" style="background: rgba(0,0,0,.3)"></div>
						<div class="container">
							<div class="row fullscreen justify-content-center align-items-center">
								<div class="col-md-10 col-12">
									<div class="banner-content text-center">
										<h4 class="text-white mb-20 text-uppercase">Discover the Colorful World</h4>
										<h1 class="text-uppercase text-white">New Trip</h1>
										<p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temp <br>
										or incididunt ut labore et dolore magna aliqua. Ut enim ad minim.</p>
										<a href="#" class="text-uppercase header-btn">Discover Now</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item-slider relative" style="background: url(<?php echo base_url(); ?>/assets/img/slider3.jpg);background-size: cover;">
						<div class="overlay" style="background: rgba(0,0,0,.3)"></div>
						<div class="container">
							<div class="row fullscreen justify-content-center align-items-center">
								<div class="col-md-10 col-12">
									<div class="banner-content text-center">
										<h4 class="text-white mb-20 text-uppercase">Discover the Colorful World</h4>
										<h1 class="text-uppercase text-white">New Experience</h1>
										<p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temp <br>
										or incididunt ut labore et dolore magna aliqua. Ut enim ad minim.</p>
										<a href="#" class="text-uppercase header-btn">Discover Now</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

   <div class="row" style="padding:0px !important; margin:0px !important;">
      <div class="col-md-12 text-center">
         <div class="section-title" data-aos="fade-right">
            <div class="well">
                  <h3><b>GESTIÓN DE HOTELES</b> </h3>
              </div>
            </div>
         </div>
      </div>

   <div data-aos="fade-left" data-aos-delay="200">
     <!-- //linea muy importate para presentar la tabla -->
      <?php echo $output; ?>
   </div>


</div>
<br>
<br>
<br>
<br>
<br>
<br>
<script type="text/javascript">
submitHandler:function(grocery_CRUD){
        var url=$(grocery_CRUD).prop("action");//capturando url (controlador/funcion)
        //generando peticion asincrona
        $.ajax({
             url:url,//action del formulario
             type:'post',//definiendo el tipo de envio de datos post/get
             data:$(grocery_CRUD).serialize(), //enviando los datos ingresados en el formulario
             success:function(data){ //funcion que se ejecuta cuando SI se realiza la peticion
                alert('Pelicula guardada exitosamente');
                gestionHoteles();//llamndo la funcion para actualizar el listado clientes
                $(form)[0].reset();
                cerrarModal();
                Swal.fire({
                  background:'#0C062E',
                  color:'#FFF',
                  title: 'Confirmacion',
                  text: "Pelicula Guardada Exitosamente",
                  icon: 'success',
                });
             },
             error:function(data){ //funcion que se ejecuta cuando NO se realiza la peticion
                alert('Error al insertar, intente nuevamente');
             }
        });
      }

</script>
