<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Turismo</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/linearicons.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/font-awesome.min.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/bootstrap.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/magnific-popup.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/animate.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/owl.carousel.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/main.css">

			<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
			<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
			<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/librerias/bootstrap3/css/bootstrap.css">
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/librerias/bootstrap3/js/bootstrap.js"></script>
			<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

			<?php if ($this->session->flashdata('confirmacion')): ?>
				<script type="text/javascript">
					$(document).ready(function(){
						Swal.fire(
							'CONFIRMACIÓN',
							'<?php echo $this->session->flashdata('confirmacion') ?>',
							'success'
						);
					});
				</script>
			<?php endif; ?>
			<?php if ($this->session->flashdata('error')): ?>
				<script type="text/javascript">
					$(document).ready(function(){
						Swal.fire(
							'ERROR',
							'<?php echo $this->session->flashdata('error') ?>',
							'error'
						);
					});
				</script>
			<?php endif; ?>
			</head>
		<body>
			<!-- start banner Area -->
			<section class="banner-area" id="home">
				<!-- Start Header Area -->
				<header class="default-header">
							<!-- <?php if ($this->session->userdata("usuarioC0nectado")): ?>
								<h6 style="color:white"><?php echo $this->session->userdata("usuarioC0nectado")['perfil']; ?></h6>
							<?php endif; ?> -->
					<nav class="navbar navbar-expand-lg  navbar-light">
						<div class="container">
							  <a class="navbar-brand" href="index.html">
							  	<img src="<?php echo base_url(); ?>/assets/img/logo.png" alt="">
							  </a>
							  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							    <span class="text-white lnr lnr-menu"></span>
							  </button>

							  <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupportedContent">
							    <ul class="navbar-nav">
										<li role="presentation" ><a href="<?php echo base_url()	?>"> Inicio </a></li>
										<?php if ($this->session->userdata("usuarioC0nectado")['perfil']=="ADMINISTRADOR"): ?>
										<li role="presentation"><a href="<?php echo site_url();?>/gestiones/gestionClientes"> Gestion  de  Cliente </a></li>
										<li role="presentation"><a href="<?php echo site_url();?>/gestionesHotel/gestionHoteles"> Gestion  de  Hoteles </a></li>
										<li role="presentation"><a href="<?php echo site_url();?>/gestionesDestino/gestionDestinos"> Gestion  de  Destinos </a></li>
										<li role="presentation"><a href="<?php echo site_url();?>/gestionesProvincia/gestionProvincias">Provincias </a></li>
										<!-- <li role="presentation"><a href="<?php echo site_url();?>/gestionesReserva/gestionReservas"> Gestion  de  Reservas </a></li> -->
										<li role="presentation"><a href="<?php echo site_url();?>/gestionesVenta/gestionVentas"> Gestion  de  Ventas </a></li>
										<!-- <li role="presentation"><a href="<?php echo site_url();?>/usuarios/listadoUsuario"> Registrarse </a></li> -->

										<!-- validar el inicio de sesion abilitando los botones, traemos la variable usuarioC0nectado -->
										<!-- <?php if ($this->session->userdata("usuarioC0nectado")): ?> -->

											<!-- funciona para ocutar funciones que no debepresentarse a un perfil -->
											<!-- <?php if ($this->session->userdata("usuarioC0nectado")['perfil']=="ADMINISTRADOR"): ?> -->

											<!-- <?php endif; ?> -->

											<!-- <?php if ($this->session->userdata("usuarioC0nectado")['perfil']=="ADMINISTRADOR"): ?>
												<li role="presentation"><a href="<?php echo site_url() ?>/generos/listadoGen"> Gestion  de  Género </a></li>
											<?php endif; ?> -->


										  <!-- <li role="presentation"><a href="<?php echo site_url() ?>/alquileres/listadoAlq"> Gestion  de  Alquileres</a></li> -->
											<li role="presentation"><a href="<?php echo site_url() ?>/usuarios/nuevo"> Registrarse</a></li>
											<?php endif; ?>
											<li  role="presentation"><a class="" href="<?php echo site_url()	?>/seguridades/cerrarSesion">
													<?php echo $this->session->userdata("usuarioC0nectado")['email']; ?>
													&nbsp;
												 Salir</a></li>
										<?php else: ?>
											<li  role="presentation"><a class="" href="<?php echo site_url()	?>/seguridades/login"> Ingresar</a></li>
										<?php endif; ?>
							    </ul>
							  </div>
						</div>
					</nav>
				</header>
				<!-- End Header Area -->
			</section>
