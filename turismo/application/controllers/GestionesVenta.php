<?php
class GestionesVenta extends CI_Controller{

    //Constructor de la clase (Ojo doble guion bajo __)
    public function __construct(){
      parent::__construct();
      //cargar la libreria en este controlador
      $this->load->database();
      $this->load->library('grocery_CRUD');

      //Verificar si existe o no alguien conectado
      // if(!$this->session->userdata("usuarioC0nectado")){
      //     $this->session->set_flashdata("error","Por favor Inicie Sesion");
      //     redirect('seguridades/cerrarSesion');
      // }else{//Codigo cuando SI esta conectado
      //   if(!($this->session->userdata("usuarioC0nectado")["perfil"]=="ADMINISTRADOR"
      //     || $this->session->userdata("usuarioC0nectado")["perfil"]=="VENDEDOR")){
      //       redirect('seguridades/cerrarSesion');
      //   }
      // }
    }

    public function gestionVentas(){
      $clientes=new grocery_CRUD();
      $clientes->set_relation('fk_id_cli','cliente','nombre_cli');
      $clientes->set_relation('fk_id_des','destino','{lugar_des}--Hotel--{fk_id_hot}');
      $clientes->set_table('venta');
      $clientes->set_language('spanish');
      $clientes->set_theme('flexigrid');
      // $clientes->set_field_upload('imagen_portada_peli','uploads/peliculas');

      // con sta linea podemos cambiar el nombre de los campos que vienen de la db
      $clientes->display_as('fk_id_cli','Cliente');
      $clientes->display_as('personas_ven','Cantidad de Personas');
      $clientes->display_as('fk_id_des','Paquete');
      $clientes->display_as('precio_ven','Precio');
      $clientes->display_as('fecha_creacion_venta','Fecha de Venta');
      // $clientes->set_subject('Cliente');
      $output=$clientes->render();
      $this->load->view('encabezado');
      $this->load->view('gestionesVenta/gestionVentas',$output);
      $this->load->view('pie');
    }




  }
 ?>
