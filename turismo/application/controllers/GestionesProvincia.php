<?php
class GestionesProvincia extends CI_Controller{

    //Constructor de la clase (Ojo doble guion bajo __)
    public function __construct(){
      parent::__construct();
      //cargar la libreria en este controlador
      $this->load->database();
      $this->load->library('grocery_CRUD');

      //Verificar si existe o no alguien conectado
      // if(!$this->session->userdata("usuarioC0nectado")){
      //     $this->session->set_flashdata("error","Por favor Inicie Sesion");
      //     redirect('seguridades/cerrarSesion');
      // }else{//Codigo cuando SI esta conectado
      //   if(!($this->session->userdata("usuarioC0nectado")["perfil"]=="ADMINISTRADOR"
      //     || $this->session->userdata("usuarioC0nectado")["perfil"]=="VENDEDOR")){
      //       redirect('seguridades/cerrarSesion');
      //   }
      // }
    }

    public function gestionProvincias(){
      $clientes=new grocery_CRUD();
      $clientes->set_relation('fk_id_hot','hotel','nombre_hot');
      // $clientes->field_type('destino_res','dropdown',array('1' => 'Quito', '2' => 'Cuenca', '3' => 'Guayaquil', '4' => 'Ambato', '5' => 'Manabi', '6' => 'Esmeraldas',));
      $clientes->set_table('provincia');
      $clientes->set_language('spanish');
      $clientes->set_theme('flexigrid');
      // $clientes->set_field_upload('imagen_portada_peli','uploads/peliculas');

      // con sta linea podemos cambiar el nombre de los campos que vienen de la db
      $clientes->display_as('nombre_pro','Provincia');
      $clientes->display_as('fk_id_hot','Hotel');
      // $clientes->display_as('direccion_hot','Direccion');
      // $clientes->display_as('precio_hot','Precio');
      // $clientes->set_subject('Cliente');
      $output=$clientes->render();
      $this->load->view('encabezado');
      $this->load->view('gestionesProvincia/gestionProvincias',$output);
      $this->load->view('pie');
    }
    //Callback que genera el combo idlocalidades


// function cbkhoteles()
//
// {
//
// //creamos el combo
// $combo = '<select name="fk_id_hot" class="chosen-select" data-placeholder="Seleccionar hotel" style="width: 300px; display: none;">';
//
// $fincombo = '</select>';
//   $clientes = new grocery_CRUD();
//   $estado = $clientes->getState();
//
//    $db = $this->db->get();
//    $row = $db->row(0);
//    //Cargamos el combo con todas las localidades de la pronvincia
//
//    $this->db->select('*')
//       ->from('provincia')
// 
//       ->where('fk_id_hot', $fk_id_hot);
//
//    $db = $this->db->get();
//
//
//
//
// //Si ecnontramos el id de localidad actual lo ponemos como selecionado
// //sino seguimos cargando las demas localidades
//
//
//    foreach($db->result() as $row):
//
//     if($row->fk_id_hot == $fk_id_hot) {
//      $combo .= '<option value="'.$row->fk_id_hot.'" selected="selected">'.$row->hotel.'</option>';
//
//     } else {
//      $combo .= '<option value="'.$row->fk_id_hot.'">'.$row->hotel.'</option>';
//
//     }
//    endforeach;
//
//
// //Devolvemos el combo cargado
//
//    return $combo.$fincombo;
//   }
//
// //Consulta de localidades
//
// function buscarhoteles()
// {
//
//    //Tomo el id de provincia que se envió como parámetro por url al seleccionar
// //una provincia del combo idprovincia
//
// $id_pro = $this->uri->segment(3);
//
//
// //consulto las localidades segun la provincia seleccionada
// $this->db->select("*")
//
//    ->from('provincia')
//    ->where('fk_id_hot', $fk_id_hot);
//
// $db = $this->db->get();
//
//
//       //Asigno la respuesta sql a un array
// $array = array();
//
// foreach($db->result() as $row):
//  $array[] = array("value" => $row->fk_id_hot, "property" => $row->hotel);
//
// endforeach;
//
//
// echo json_encode($array);
// exit;
//
// }
  }
 ?>
