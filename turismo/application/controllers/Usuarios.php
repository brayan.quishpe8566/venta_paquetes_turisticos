<?php
/**
 *
 */
class Usuarios extends CI_Controller
{

  public function __construct()
  {
    parent:: __construct();
    $this->load->model('usuario');

    if (!$this->session->userdata("usuarioC0nectado")) {
      $this->session->set_flashdata("error","Inicie session");
      redirect('seguridades/cerrarSesion');
    }
  }

  public function nuevo(){
    $this->load->view('encabezado');
    $this->load->view('usuarios/nuevo');
    $this->load->view('pie');
  }

  public function insertarUsuario(){
    $password=$this->input->post('password_usu');
    $dataUsuario=array(
      'correo_usu'=>$this->input->post("correo_usu"),
      'password_usu'=>$password,
      'perfil_usu'=>$this->input->post('perfil_usu')
      // 'estado_usu'=>$this->input->posy('1')
    );
    if ($this->usuario->insertarUsuario($dataUsuario)) {
      $this->session->set_flashdata('confirmacion','Usuarioregistrado');
      redirect('/usuarios/nuevo');
    } else {
      $this->session->set_flashdata('error','Error de registro');
      redirect('usuarios/nuevo');
    }
  }

  public function listadoUsuario(){
    $data["listadoUsuarios"]=$this->usuario->obtenerDatos();
    $this->load->view('encabezado');
    $this->load->view('usuarios/listado',$data);
    $this->load->view('pie');
  }

  public function eliminarUsuario($id){
    if ($this->usuario->eliminarUsuario($id)) {
      $this->session->set_flashdata("confirmacion","Usuario eliminado exitosamnte");
      redirect('usuarios/listadoUsuario');
    } else {
      echo "Error al eliminar";
    }

  }

  public function editarUsuario($id){
    $data["editarUsuarios"]=$this->usuario->obtenerPorId($id);
    $this->load->view('encabezado');
    $this->load->view('usuarios/editar',$data);
    $this->load->view('pie');
  }

  public function actualizarUsuario(){
    $id_usu=$this->input->post('id_usu');
    $password=$this->input->post('password_usu');
    $datosEditados=array(
      'correo_usu'=>$this->input->post('correo_usu'),
      'password_usu'=>md5($password),
      'perfil_usu'=>$this->input->post('perfil_usu')
      // 'estado_usu'=>$this->input->post('estado_usu')
    );

    if ($this->usuario->actualizarUsuario($id_usu, $datosEditados)) {
      $this->session->set_flashdata('confirmacion','Usuario actualizado');
      redirect('usuarios/listadoUsuario');
    } else {
      echo "error al actualizar";
    }

  }

}

?>
