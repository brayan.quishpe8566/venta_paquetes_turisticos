<?php
class GestionesReserva extends CI_Controller{

    //Constructor de la clase (Ojo doble guion bajo __)
    public function __construct(){
      parent::__construct();
      //cargar la libreria en este controlador
      $this->load->database();
      $this->load->library('grocery_CRUD');

      //Verificar si existe o no alguien conectado
      // if(!$this->session->userdata("usuarioC0nectado")){
      //     $this->session->set_flashdata("error","Por favor Inicie Sesion");
      //     redirect('seguridades/cerrarSesion');
      // }else{//Codigo cuando SI esta conectado
      //   if(!($this->session->userdata("usuarioC0nectado")["perfil"]=="ADMINISTRADOR"
      //     || $this->session->userdata("usuarioC0nectado")["perfil"]=="VENDEDOR")){
      //       redirect('seguridades/cerrarSesion');
      //   }
      // }
    }

    public function gestionReservas(){
      $clientes=new grocery_CRUD();
      $clientes->callback_add_field('fk_id_hot', array($this, 'cbkhoteles'));

      $clientes->callback_edit_field('fk_id_hot', array($this, 'cbkhoteles'));

      // $clientes->field_type('destino_res','dropdown',array('1' => 'Quito', '2' => 'Cuenca', '3' => 'Guayaquil', '4' => 'Ambato', '5' => 'Manabi', '6' => 'Esmeraldas',));
      $clientes->set_table('reserva');
      $clientes->set_language('spanish');
      $clientes->set_theme('flexigrid');
      // $clientes->set_field_upload('imagen_portada_peli','uploads/peliculas');

      // con sta linea podemos cambiar el nombre de los campos que vienen de la db
      // $clientes->display_as('fk_id_cli','Cliente');
      // $clientes->display_as('destino_res','Destino');
      $clientes->display_as('fk_id_cli','Cliente');
      $clientes->display_as('fk_id_des','Destino');
      $clientes->display_as('precio_res','Precio');
      $clientes->display_as('fecha_res','Fecha de Reservacion');
      $clientes->display_as('fecha_fin_res','Fecha fin de la Reservacion');
      $clientes->display_as('fecha_creacion_res','Fecha de la Agendacion');
      $clientes->display_as('fk_id_usu','Usuario');
      // $clientes->set_subject('Cliente');
      $output=$clientes->render();
      $this->load->view('encabezado');
      $this->load->view('gestionesReserva/gestionReservas',$output);
      $this->load->view('pie');
    }
    //Callback que genera el combo idlocalidades

  }
 ?>
