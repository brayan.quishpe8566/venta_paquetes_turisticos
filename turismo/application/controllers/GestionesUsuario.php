<?php
class GestionesUsuario extends CI_Controller{

    //Constructor de la clase (Ojo doble guion bajo __)
    public function __construct(){
      parent::__construct();
      //cargar la libreria en este controlador
      $this->load->database();
      $this->load->library('grocery_CRUD');

      //Verificar si existe o no alguien conectado
      // if(!$this->session->userdata("usuarioC0nectado")){
      //     $this->session->set_flashdata("error","Por favor Inicie Sesion");
      //     redirect('seguridades/cerrarSesion');
      // }else{//Codigo cuando SI esta conectado
      //   if(!($this->session->userdata("usuarioC0nectado")["perfil"]=="ADMINISTRADOR"
      //     || $this->session->userdata("usuarioC0nectado")["perfil"]=="VENDEDOR")){
      //       redirect('seguridades/cerrarSesion');
      //   }
      // }
    }

    public function gestionUsuarios(){
      $clientes=new grocery_CRUD();
      $clientes->field_type('perfil_usu','dropdown',array('1'=>  'SECRETARI@','2'=>  'ADMINISTRADOR',));
      // $clientes->field_type('perfil_usu','select',option('1'=>  'SECRETARI@','disabled="disabled"'=>  ('ADMINISTRADOR').prop( "disabled", true ),));

      // $clientes -> display_as ( 'perfil_usu','dropdown',array('1'=>  'ADMINISTRADOR' ))   ;
      $clientes->set_table('usuario');
      $clientes->set_language('spanish');
      $clientes->set_theme('flexigrid');
      // $clientes->set_field_upload('imagen_portada_peli','uploads/peliculas');

      // con sta linea podemos cambiar el nombre de los campos que vienen de la db
      $clientes->display_as('nombre_usu','Nombre');
      $clientes->display_as('apellido_usu','Apellido');
      $clientes->display_as('nacionalidad_usu','Nacionalidad');
      $clientes->display_as('correo_usu','Correo');
      $clientes->display_as('password_usu','Contraseña');
      $clientes->display_as('perfil_usu','Perfil');
      // $clientes->set_subject('Cliente');
      $output=$clientes->render();
      $this->load->view('encabezado');
      $this->load->view('gestionesUsuario/gestionUsuarios',$output);
      $this->load->view('pie');
    }




  }
 ?>
