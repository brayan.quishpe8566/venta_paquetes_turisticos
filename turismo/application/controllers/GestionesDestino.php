<?php
class GestionesDestino extends CI_Controller{

    //Constructor de la clase (Ojo doble guion bajo __)
    public function __construct(){
      parent::__construct();
      //cargar la libreria en este controlador
      $this->load->database();
      $this->load->library('grocery_CRUD');

      //Verificar si existe o no alguien conectado
      // if(!$this->session->userdata("usuarioC0nectado")){
      //     $this->session->set_flashdata("error","Por favor Inicie Sesion");
      //     redirect('seguridades/cerrarSesion');
      // }else{//Codigo cuando SI esta conectado
      //   if(!($this->session->userdata("usuarioC0nectado")["perfil"]=="ADMINISTRADOR"
      //     || $this->session->userdata("usuarioC0nectado")["perfil"]=="VENDEDOR")){
      //       redirect('seguridades/cerrarSesion');
      //   }
      // }
    }

    public function gestionDestinos(){
      $clientes=new grocery_CRUD();
      $clientes->set_relation('fk_id_hot','hotel','Nombre: {nombre_hot}--Dirección: {direccion_hot}--Precio: ${precio_hot}');
      $clientes->set_table('destino');
      $clientes->set_language('spanish');
      $clientes->set_theme('flexigrid');
      // $clientes->set_field_upload('imagen_portada_peli','uploads/peliculas');

      // con sta linea podemos cambiar el nombre de los campos que vienen de la db
      $clientes->display_as('id_des','ID');
      $clientes->display_as('fk_id_hot','Hotel');
      $clientes->display_as('lugar_des','Destino');
      $clientes->display_as('precio_des','Precio');
      $clientes->display_as('fechaInicio_des','Fecha de Salida');
      $clientes->display_as('fechaFin_des','Fecha de Arrivo');
      // $clientes->set_subject('Cliente');
      $output=$clientes->render();
      $this->load->view('encabezado');
      $this->load->view('gestionesDestino/gestionDestinos',$output);
      $this->load->view('pie');
    }




  }
 ?>
