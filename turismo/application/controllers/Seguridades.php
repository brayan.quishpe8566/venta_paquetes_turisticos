<?php
    class Seguridades extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model('usuario');
        }

        public function login(){
          $this->load->view('encabezado');
          $this->load->view('seguridades/login');
          $this->load->view('pie');
        }

        public function autenticarUsuario(){

          // enviarEmail("ariel.12isaias@gmail.com",
          //             "SALUDO_2",
          //             "<h1 style='color:blue;'>Usted a ingresado al sistema , </h1> <br>  ".date('Y-m-d H:i:s'),
          //             "");
          $email=$this->input->post('correo_usu');
          $password=$this->input->post('password_usu');
          $usuarioConsultado=$this->usuario->consultarPorEmailPassword($email,$password);


          if ($usuarioConsultado) {
            // echo "Email y contraseña correctos";
            //crear una variable de sesion -> user data

            $datosSesion=array(
              //podemos sacar todos los datos necesarios
              "id"=>$usuarioConsultado->id_usu,
              "email"=>$usuarioConsultado->correo_usu,
              "perfil"=>$usuarioConsultado->perfil_usu
            );
            //el nombre del user data debe tener seguridad
            $this->session->set_userdata("usuarioC0nectado",$datosSesion);
            $this->session->set_flashdata("confirmacion","Acceso exitoso, bien venido al sistema");
            redirect("/");

          }else {
            $this->session->set_flashdata("error","Email o contraseña incorrectos");
            redirect('seguridades/login');
          }

        }
        //esta funciona se llama en la vista para cerra sesion
        public function cerrarSesion(){
          $this->session->sess_destroy();//borra todas la sessiones ingresadas
          redirect("seguridades/login");
        }
    }
?>
